export default{
    items(state){
        return state.items;
    },
    filteredItems(state){
        return state.filteredItemsByName;
    }
}