const filterByType = (state, payload) => {
  return state.items.filter((item) => item.type === payload);
};

export default {
  setItems(state, payload) {
    state.items = payload;
    state.filteredItems = payload;
    state.filteredItemsByName = payload;
  },
  filterItemsByType(state, payload) {
    if (!payload || payload === "Todos") {
      state.filteredItemsByName = state.items;
      state.filteredItemsByType = state.items;
      state.type = "Todos";
    } else {
      state.filteredItemsByType = filterByType(state, payload);
      state.filteredItemsByName = filterByType(state, payload);
      state.type = payload;
    }
  },
  filterItemsByName(state, payload) {
    if (payload || payload === "") {
      state.filteredItemsByName = state.filteredItemsByType.filter((item) =>
        item.description.toLowerCase().includes(payload.toLowerCase())
      );
    } else {
      state.filteredItemsByName = state.filteredItemsByType;
    }
  },
};
