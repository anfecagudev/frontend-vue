import useHttp from "../../../hooks/http.js";

export default {
  async fetch(context) {
    const { sendRequest } = useHttp();
     const data = await sendRequest(
      {
        url: "http://localhost:8081/users/datalist",
      },
      "Fetching items from database"
    );
    
    return context.dispatch('setItems', data)
  },
  setItems(context, payload) {
    context.commit("setItems", payload);
  },
  filterItemsByType(context, payload){
    context.commit("filterItemsByType", payload);
  },
  filterItemsByName(context, payload){
    context.commit("filterItemsByName", payload);
  }
};
