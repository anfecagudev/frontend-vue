const addtoCounter = (state) => {
  return (state.counter = state.cartItems.reduce(
    (prev, cur) => prev + cur.quantity,
    0
  ));
};

export default {
  openModal(state, payload) {
    state.item = payload;
    state.modal = "detail";
  },
  closeModal(state) { 
    state.premodal = true;
    setTimeout(() => {
      state.modal = null;
      state.premodal = false;
    }, 300);
  },
  addToCart(state, payload) {
    if (
      state.cartItems.filter((item) => item.item.code === payload.item.code)
        .length > 0
    ) {
      state.cartItems.find(
        (item) => item.item.code === payload.item.code
      ).quantity += payload.quantity;
    } else {
      state.cartItems.push({
        item: payload.item,
        quantity: payload.quantity,
      });
    }
    addtoCounter(state);
  },
  setCart(state, payload) {
    state.cartItems = payload;
    addtoCounter(state);
  },
  addToItem(state, payload) {
    state.cartItems.find((item) => item.item.code === payload.item.code)
      .quantity++;
    addtoCounter(state);
  },
  minusToItem(state, payload) {
    if (payload.quantity === 1) {
      state.cartItems = state.cartItems.filter(
        (item) => item.item.code !== payload.item.code
      );
    } else {
      state.cartItems.find((item) => item.item.code === payload.item.code)
        .quantity--;
    }
    addtoCounter(state);
  },
  openCartModal(state) {
    state.modal = "cart";
  },
  openSpinner(state, payload) {
    state.spinner = true;
    state.spinnerMessage = payload;
  },
  closeSpinner(state) {
    state.spinner = false;
    state.spinnerMessage = null;
  },
};
