import getters from "./getters.js";
import mutations from "./mutations.js";
import actions from "./actions.js";

export default {
  namespaced: true,
  state() {
    return {
      modal: null,
      item: {},
      cartItems: [],
      counter: 0,
      spinner: false,
      spinnerMessage: null,
      premodal:null
    };
  },
  getters,
  mutations,
  actions,
};
