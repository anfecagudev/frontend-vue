export default {
  modal(state) {
    return state.modal;
  },
  item(state) {
    return state.item;
  },
  cartItems(state) {
    return state.cartItems;
  },
  counter(state) {
    return state.counter;
  },
  spinner(state) {
    return state.spinner;
  },
  spinnerMessage(state) {
    return state.spinnerMessage;
  },
  premodal(state){
    return state.premodal;
  }
};
