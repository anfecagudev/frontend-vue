import useHttp from "../../../hooks/http";

const { sendRequest } = useHttp();

export default {
  openModal(context, payload) {
    context.commit("openModal", payload);
  },
  closeModal(context) {
    context.commit("closeModal");
  },
  addToCart(context, payload) {
    context.commit("addToCart", payload);
  },
  setCart(context, payload) {
    context.commit("setCart", payload);
  },
  addToItem(context, payload) {
    context.commit("addToItem", payload);
  },
  minusToItem(context, payload) {
    context.commit("minusToItem", payload);
  },
  openCartModal(context) {
    context.commit("openCartModal");
  },
  openSpinner(context, payload) {
    context.commit("openSpinner", payload);
  },
  closeSpinner(context) {
    context.commit("closeSpinner");
  },
  async fetchCart(context, payload) {
    context.commit("openSpinner", "Fetching User Cart");
    const data = await sendRequest(
      {
        url: "http://localhost:8081/cart/cart",
        headers: {
          Authorization: `Bearer ${payload}`,
        },
      },
      "Fetching User Cart"
    );

    const transformedData = data.products.map((dataItem) => {
      const { productItem: item, quantity } = dataItem;
      return { item, quantity };
    });

    context.commit("closeSpinner");

    return context.dispatch("setCart", transformedData);
  },
  async saveCart(context, payload) {
    context.commit("openSpinner", "Saving User Cart...");

    await sendRequest({
      url: "http://localhost:8081/cart/savecart",
      method: "POST",
      headers: {
        Authorization: `Bearer ${payload.token}`,
        "Content-Type": "application/json",
      },
      body: payload.items,
    });

    context.commit("closeSpinner");

    return context.dispatch("closeModal");
  },
};
