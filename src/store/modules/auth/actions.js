

export default {
    authenticate(context, payload){
        context.commit('authenticate', payload);
    },
    logout(context){
        context.commit("logout");
    }
}