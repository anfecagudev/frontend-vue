export default {
  authenticate(state, payload) {
    state.isAuthenticated = true;
    state.token = payload.token;
    console.log(payload.imgUrl)
    console.log(payload.token)
    console.log(payload.userName)
    state.imgUrl = payload.imgUrl;
    state.userName = payload.userName;
    state.error = null;
    state.isError = false;
  },
  logout(state){
    state.isAuthenticated = false;
    state.token = null;
    state.imgUrl = null;
    state.userName = null;
    state.error = null;
    state.isError = false;
}
};
