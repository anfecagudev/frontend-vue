import getters from "./getters.js";
import mutations from "./mutations.js";
import actions from "./actions.js";

export default {
  namespaced: true,
  state() {
    return {
      isAuthenticated: false,
      token: null,
      imgUrl: null,
      userName: null,
      isError: false,
      error: null,
    };
  },
  getters,
  mutations,
  actions,
};
