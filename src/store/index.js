import { createStore } from 'vuex';
import authModule from './modules/auth/index.js';
import itemsModule from './modules/items/index.js';
import cartModule from './modules/cart/index.js';

const store = createStore({
    modules: {
        auth: authModule,
        cart: cartModule,
        items: itemsModule
    }
})


export default store;