import { createApp } from "vue";
import App from "./App.vue";
import router from "./router.js";
import store from "./store/index.js";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import Button from "./components/UI/Button.vue";
import Input from "./components/UI/Input.vue";

const app = createApp(App);

library.add(fas);
app.use(router);
app.use(store);
app.component("fa", FontAwesomeIcon);
app.component('base-button', Button);
app.component('base-input', Input);
app.mount("#app");
