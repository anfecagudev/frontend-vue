import { useStore } from "vuex";

export default function useAutoLogin(token) {
  const store = useStore();

  let timer;

  const payload = JSON.parse(
    decodeURIComponent(escape(window.atob(token.split(".")[1])))
  );
  store.dispatch("auth/authenticate", {
    token: token,
    imgUrl: payload.imgUrl,
    userName: payload.username,
  });
  const remainingTime = payload.exp * 1000 - new Date().getTime();
  timer = setTimeout(() => {
    localStorage.removeItem("token");
    store.dispatch("auth/logout");
  }, remainingTime);
  store.dispatch("cart/fetchCart", token);

  return timer;
}
