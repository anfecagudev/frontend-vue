import { useStore } from "vuex";
import { useRouter } from "vue-router";

export default function useRedirect() {
  const store = useStore();

  const router = useRouter();

  const redirectTo = (destiny, message) => {
    store.dispatch[("cart/openSpinner", message)];
    setTimeout(() => {
      store.dispatch["auth/closeSpinner"];
      if (destiny) router.push(destiny);
    }, 1000);
  };

  return redirectTo;
}
