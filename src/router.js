import { createRouter, createWebHistory } from "vue-router";
import Main from "./pages/Main.vue";
import Callback from './pages/Callback.vue';

const url =
  "http://localhost:8081/oauth2/authorize/google?redirect_uri=http://localhost:8080/callback";

const auth = () => {
  window.location.href = url;
  return null;
};

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect:"/welcome"},
    { path: "/welcome", component: Main },
    { path: "/googleauth", component: auth },
    { path: "/callback", component: Callback}
  ],
});

export default router;
